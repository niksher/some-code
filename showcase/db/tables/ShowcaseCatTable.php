<?php

namespace progorod\modules\specproject\showcase\db\tables;

use progorod\db\IDb;
use progorod\db\schema\Schema;
use progorod\db\schema\SchemaBuilder;


class ShowcaseCatTable
{
    const NAME = "showcase_cats";

    /**
     * @return ShowcaseCatTableFields
     */
    public static function fields()
    {
        return new ShowcaseCatTableFields();
    }

    /**
     * @return Schema
     */
    public static function schema()
    {
        $f = self::fields();

        return (new SchemaBuilder())
            ->setTable(self::NAME)

            ->columnAutoincr($f->id, "INT")
            ->column($f->isActive, "TINYINT", 1, 0)
            ->column($f->articleId, "INT", 0, 0)
            ->column($f->name, "LONGTEXT", 0, "")

            ->primaryKey($f->id)
            ->index($f->id)

            ->setEngine("InnoDb")
            ->getSchema();
    }


    public function __construct( IDb $db )
    {
        $this->_db = $db;
    }


    /** @var IDb */
    private $_db;


    /**
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @return ShowcaseCatTableFields
     */
    public function getFields()
    {
        return self::fields();
    }

    /**
     * @return Schema
     */
    public function getSchema()
    {
        return self::schema();
    }
}
