<?php

namespace progorod\modules\specproject\showcase\db\tables;


class ShowcaseTableFields
{
    public $id = "id";
    public $created = "created";
    public $createdBy = "createdBy";
    public $modified = "modified";
    public $modifiedBy = "modifiedBy";
    public $isActive = "isActive";
    public $date = "date";
    public $name = "name";
    public $content = "content";
    public $pageTitle = "pageTitle";
    public $pageDescription = "pageDescription";
    public $imagePath = "imagePath";
    public $style = "style";
}
