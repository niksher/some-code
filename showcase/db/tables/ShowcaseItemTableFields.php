<?php

namespace progorod\modules\specproject\showcase\db\tables;


class ShowcaseItemTableFields
{
    public $id = "id";
    public $isActive = "isActive";
    public $catId = "catId";
    public $name = "name";
    public $imagePath = "imagePath";
    public $content = "content";
    public $url = "url";
}
