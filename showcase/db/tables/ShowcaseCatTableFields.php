<?php

namespace progorod\modules\specproject\showcase\db\tables;


class ShowcaseCatTableFields
{
    public $id = "id";
    public $isActive = "isActive";
    public $articleId = "articleId";
    public $name = "name";
}
