<?php

namespace progorod\modules\specproject\showcase\db\tables;

use progorod\db\IDb;
use progorod\db\schema\Schema;
use progorod\db\schema\SchemaBuilder;


class ShowcaseTable
{
    const NAME = "showcase";

    /**
     * @return ShowcaseTableFields
     */
    public static function fields()
    {
        return new ShowcaseTableFields();
    }

    /**
     * @return Schema
     */
    public static function schema()
    {
        $f = self::fields();

        return (new SchemaBuilder())
            ->setTable(self::NAME)

            ->columnAutoincrInt($f->id)
            ->columnInt($f->created)
            ->columnInt($f->createdBy)
            ->columnInt($f->modified)
            ->columnInt($f->modifiedBy)
            ->columnInt($f->date)
            ->column($f->isActive, "TINYINT", 1, 0)
            ->columnVarchar($f->name, 255)
            ->columnVarchar($f->pageDescription, 255)
            ->columnVarchar($f->pageTitle, 255)
            ->columnText($f->content)
            ->columnVarchar($f->imagePath, 255)
            ->columnVarchar($f->style, 255, "green")

            ->primaryKey($f->id)
            ->index($f->id)



            ->setEngine("InnoDb")
            ->getSchema();
    }


    public function __construct( IDb $db )
    {
        $this->_db = $db;
    }


    /** @var IDb */
    private $_db;


    /**
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @return ShowcaseTableFields
     */
    public function getFields()
    {
        return self::fields();
    }

    /**
     * @return Schema
     */
    public function getSchema()
    {
        return self::schema();
    }
}
