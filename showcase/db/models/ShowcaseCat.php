<?php

namespace progorod\modules\specproject\showcase\db\models;


class ShowcaseCat
{
    /** @var int */
    public $id;

    /** @var boolean */
    public $isActive;

    /** @var int */
    public $articleId;

    /** @var string */
    public $name;

    /** @var ShowcaseItem[] */
    public $items;
}
