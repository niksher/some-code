<?php

namespace progorod\modules\specproject\showcase\db\models;


class ShowcaseItemSelector
{
    /**
     * @return ShowcaseItemSelector
     */
    public static function me()
    {
        return new ShowcaseItemSelector();
    }

    /**
     * @param type $array
     * @return ShowcaseItem[]
     */
    public function fetch( $array )
    {
        return $this->_fetch($array);
    }

    /**
     * @param type $array
     * @return ShowcaseItem
     */
    public function fetchOne( $array )
    {
        return $this->_fetch($array, true);
    }

    /**
     * @param array $array
     * @param boolean $one
     * @return ShowcaseItem
     */
    private function _fetch( $array, $one=false )
    {
        $fetch = [];
        foreach ($array as $arr) {
            $o = new ShowcaseItem();
            $o->id = (int)$arr["id"];
            $o->catId = (int)$arr["catId"];
            $o->isActive = (int)$arr["isActive"];
            $o->content = $arr["content"];
            $o->imagePath = $arr["imagePath"];
            $o->name = $arr["name"];
            $o->url = $arr["url"];

            if ($one) {
                return $o;
            }
            $fetch[] = $o;
        }
        return $fetch;
    }
}
