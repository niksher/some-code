<?php

namespace progorod\modules\specproject\showcase\db\models;


class ShowcaseSelector
{
    /**
     * @return ShowcaseSelector
     */
    public static function me()
    {
        return new ShowcaseSelector();
    }

    /**
     * @param type $array
     * @return Showcase[]
     */
    public function fetch( $array )
    {
        return $this->_fetch($array);
    }

    /**
     * @param type $array
     * @return Showcase
     */
    public function fetchOne( $array )
    {
        return $this->_fetch($array, true);
    }

    /**
     * @param array $array
     * @return Showcase
     */
    private function _fetch( $array, $one=false )
    {
        $fetch = [];
        foreach ($array as $arr) {
            $o = new Showcase();
            $o->id = (int)$arr["id"];
            $o->created = (int)$arr["created"];
            $o->createdBy = (int)$arr["createdBy"];
            $o->modified = (int)$arr["modified"];
            $o->modifiedBy = (int)$arr["modifiedBy"];
            $o->date = (int)$arr["date"];
            $o->isActive = (int)$arr["isActive"];
            $o->content = $arr["content"];
            $o->name = $arr["name"];
            $o->pageDescription = $arr["pageDescription"];
            $o->pageTitle = $arr["pageTitle"];
            $o->imagePath = $arr["imagePath"];
            $o->style = $arr["style"];

            if ($one) {
                return $o;
            }
            $fetch[] = $o;
        }
        return $fetch;
    }
}

