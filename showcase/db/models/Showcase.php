<?php

namespace progorod\modules\specproject\showcase\db\models;


class Showcase
{
    /** @var int */
    public $id;

    /** @var int */
    public $created;

    /** @var int */
    public $createdBy;

    /** @var int */
    public $modified;

    /** @var int */
    public $modifiedBy;

    /** @var boolean */
    public $isActive;

    /** @var int */
    public $date;

    /** @var string */
    public $name;

    /** @var string */
    public $content;

    /** @var string */
    public $pageTitle;

    /** @var string */
    public $pageDescription;

    /** @var ShowcaseCat[] */
    public $resolvedCats = null;

    /** @var string */
    public $imagePath;

    /** @var string */
    public $style;
}
