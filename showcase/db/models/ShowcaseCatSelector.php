<?php

namespace progorod\modules\specproject\showcase\db\models;


class ShowcaseCatSelector
{
	/**
	 * @return ShowcaseCatSelector
	 */
	public static function me()
	{
		return new ShowcaseCatSelector();
	}
	
	/**
	 * @param type $array
	 * @return ShowcaseCat[]
	 */
	public function fetch( $array )
	{
		return $this->_fetch($array);
	}
	
	/**
	 * @param type $array
	 * @return ShowcaseCat
	 */
	public function fetchOne( $array )
	{
		return $this->_fetch($array, true);
	}
	
	/**
	 * @param array $array
	 * @param boolean $one
	 * @return ShowcaseCat
	 */
	private function _fetch( $array, $one=false )
	{
		$fetch = [];
		foreach ($array as $arr) {
			$o = new ShowcaseCat();
			$o->id = (int)$arr["id"];
			$o->articleId = (int)$arr["articleId"];
			$o->name = $arr["name"];
			$o->isActive = (int)$arr["isActive"];
			
			if ($one) {
				return $o;
			}
			$fetch[] = $o;
		}
		return $fetch;
	}
} 
