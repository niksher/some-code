<?php

namespace progorod\modules\specproject\showcase\db\models;


class ShowcaseItem
{
    /** @var int */
    public $id;

    /** @var boolean */
    public $isActive;

    /** @var int */
    public $catId;

    /** @var string */
    public $name;

    /** @var string */
    public $imagePath;

    /** @var string */
    public $content;

    /** @var string */
    public $url;
}
