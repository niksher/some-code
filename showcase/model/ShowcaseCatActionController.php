<?php

namespace progorod\modules\specproject\showcase\model;

use progorod\Cms;
use progorod\db\queries\DbQuery;
use progorod\db\queries\DbQuerySelect;
use progorod\modules\specproject\showcase\db\models\Showcase;
use progorod\modules\specproject\showcase\db\models\ShowcaseCat;
use progorod\modules\specproject\showcase\db\models\ShowcaseCatSelector;
use progorod\modules\specproject\showcase\db\tables\ShowcaseCatTable;
use progorod\modules\specproject\showcase\db\tables\ShowcaseCatTableFields;
use progorod\modules\specproject\showcase\ShowcaseModule;


class ShowcaseCatActionController
{
    public function __construct( ShowcaseModule $module )
    {
        $this->_module = $module;
    }


    /** @var ShowcaseModule */
    private $_module;



    /**
     * @param int $id
     * @param int $articleId
     * @param boolean $buffered
     * @return Showcase
     */
    public function selectCatById($id, $articleId=0, $buffered=false)
    {
        $f = new ShowcaseCatTableFields();
        $select = $this->_selectCatList($articleId)
            ->whereField($f->id, "=", $id);
        $arr = Cms::me()->db()->queryAssoc($select, $buffered);
        return ShowcaseCatSelector::me()->fetchOne($arr);
    }

    /**
     * @param int $articleId
     * @return ShowcaseCat
     */
    public function createCat($articleId)
    {
        $f = new ShowcaseCatTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            INSERT INTO `:table`
            SET 
                `:f_articleId` = :articleId
                , `:f_isActive` = :isActive
_____
            , [
                "table" => ShowcaseCatTable::NAME
                , "f_isActive" => $f->isActive
                , "f_articleId" => $f->articleId
                , "isActive" => 0
                , "articleId" => $articleId
            ]
            , false);
        return Cms::me()->modules()->showcase()->showcaseCatActions()->selectCatById(Cms::me()->db()->lastInsertId(), $articleId);
    }


    /**
     * @param ShowcaseCat $showcase
     */
    public function updateCat( ShowcaseCat $showcase )
    {		
        $f = new ShowcaseCatTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            UPDATE `:table`
            SET
                `:f_isActive` = :isActive
                , `:f_name` = :name
            WHERE `:f_id` = :id
_____
            , [
                "table" => ShowcaseCatTable::NAME
                , "f_id" => $f->id
                , "f_isActive" => $f->isActive
                , "f_name" => $f->name
                , "id" => $showcase->id
                , "isActive" => $showcase->isActive
                , "name" => $showcase->name
            ]
            , false);
    }

    /**
     * @param int $id
     */
    public function removeById($id)
    {
        $s = Cms::me()->modules()->showcase();
        $showcaseCat = $this->selectCatById($id);
        $showcase = $s->showcaseActions()->selectById($showcaseCat->id);
        $s->showcaseItemActions()->selectFullArticle($showcase);
        $s->showcaseItemActions()->removeByCatId($showcase, $id);
        $this->_deleteById($id);
    }

    /**
     * @param int $id
     */
    private function _deleteById( $id )
    {		
        $f = new ShowcaseCatTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            DELETE FROM `:table`
            WHERE `:f_id` = :id
_____
            , [
                "table" => ShowcaseCatTable::NAME
                , "f_id" => $f->id
                , "id" => $id
            ]
            , false);
    }


    /**
     * @param int $articleId
     * @return DbQuerySelect
     */
    public function _selectCatList($articleId)
    {
        $s = ShowcaseCatTable::fields();
        $result = DbQuery::select()
            ->select([
                $s->id 
                , $s->isActive
                , $s->articleId
                , $s->name
            ])

            ->fromTable(ShowcaseCatTable::NAME);

        if ($articleId) {
            $result->whereField($s->articleId, "=", $articleId);
        }

        return $result;
    }

}
