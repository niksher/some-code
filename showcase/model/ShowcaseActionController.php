<?php

namespace progorod\modules\specproject\showcase\model;

use model\images\Images;
use progorod\Cms;
use progorod\db\queries\DbQuery;
use progorod\db\queries\DbQuerySelect;
use progorod\helpers\Path;
use progorod\modules\specproject\showcase\db\models\Showcase;
use progorod\modules\specproject\showcase\db\models\ShowcaseSelector;
use progorod\modules\specproject\showcase\db\tables\ShowcaseTable;
use progorod\modules\specproject\showcase\db\tables\ShowcaseTableFields;
use progorod\modules\specproject\showcase\ShowcaseModule;


class ShowcaseActionController
{
    public function __construct( ShowcaseModule $module )
    {
        $this->_module = $module;
    }

    /** @var ShowcaseModule */
    private $_module;

    /**
     * @param string $sortKey
     * @param boolean $sortAsc
     * @param boolean $buffered
     * @return Showcase
     */
    public function selectList($sortKey, $sortAsc, $buffered=false)
    {
        $select = $this->_selectList($sortKey, $sortAsc);
        $arr = Cms::me()->db()->queryAssoc($select, $buffered);
        return ShowcaseSelector::me()->fetch($arr);
    }

    /**
     * @param int $id
     * @param boolean $buffered
     * @return Showcase
     */
    public function selectById($id, $buffered=false)
    {
        if (!$id) {
            return;
        }
        $f = new ShowcaseTableFields();
        $select = $this->_selectList()
            ->whereField($f->id, "=", $id);
        $arr = Cms::me()->db()->queryAssoc($select, $buffered);
        return ShowcaseSelector::me()->fetchOne($arr);
    }


    /**
     * @param int $id
     * @return Showcase
     */
    public function create()
    {
        $f = new ShowcaseTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            INSERT INTO `:table`
            SET 
                `:f_created` = :created
                , `:f_createdBy` = :createdBy
                , `:f_isActive` = :isActive
_____
            , [
                "table" => ShowcaseTable::NAME
                , "f_created" => $f->created
                , "f_createdBy" => $f->createdBy
                , "f_isActive" => $f->isActive
                , "created" => time()
                , "createdBy" => Cms::me()->modules()->auth()->session()->userId()
                , "isActive" => 0
            ]
            , false);
        return Cms::me()->modules()->showcase()->showcaseActions()->selectById(Cms::me()->db()->lastInsertId());
    }


    /**
     * @param Showcase $showcase
     */
    public function update( Showcase $showcase )
    {		
        $f = new ShowcaseTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
			UPDATE `:table`
			SET
				`:f_date` = :date
				, `:f_isActive` = :isActive
				, `:f_name` = :name
				, `:f_content` = :content
				, `:f_pageTitle` = :pageTitle
				, `:f_pageDescription` = :pageDescription
				, `:f_imagePath` = :imagePath
				, `:f_style` = :style
			WHERE `:f_id` = :id
_____
            , [
                "table" => ShowcaseTable::NAME
                , "f_id" => $f->id
                , "f_date" => $f->date
                , "f_isActive" => $f->isActive
                , "f_name" => $f->name
                , "f_content" => $f->content
                , "f_pageTitle" => $f->pageTitle
                , "f_pageDescription" => $f->pageDescription
                , "f_imagePath" => $f->imagePath
                , "f_style" => $f->style
                , "id" => $showcase->id
                , "date" => $showcase->date
                , "isActive" => $showcase->isActive
                , "name" => $showcase->name
                , "content" => $showcase->content
                , "pageTitle" => $showcase->pageTitle
                , "pageDescription" => $showcase->pageDescription
                , "imagePath" => $showcase->imagePath
                , "style" => $showcase->style
            ]
            , false);
    }

    /**
     * @param int $id
     */
    public function removeById( $id )
    {
        $showcase = $this->selectById($id);
        Cms::me()->modules()->showcase()->showcaseItemActions()->selectFullArticle($showcase);
        foreach ($showcase->resolvedCats as $cat) {
            Cms::me()->modules()->showcase()->showcaseItemActions()->removeByCatId($showcase, $cat->id);
        }
        $this->_deleteById($id);
        Path::deleteFile($showcase->imagePath);

    }

    /**
     * @param int $id
     */
    private function _deleteById( $id )
    {		
        $f = new ShowcaseTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            DELETE FROM `:table`
            WHERE `:f_id` = :id
_____
            , [
                "table" => ShowcaseTable::NAME
                , "f_id" => $f->id
                , "id" => $id
            ]
            , false);
    }


    /**
     * @param string $sortKey
     * @param boolean $sortAsc
     * @return DbQuerySelect
     */
    private function _selectList($sortKey="created", $sortAsc="ASC")
    {
        $sortType = $sortAsc ? "ASC" : "DESC"; 
        $s = ShowcaseTable::fields();
        $result = DbQuery::select()
            ->select([
                $s->id 
                , $s->content
                , $s->created
                , $s->createdBy
                , $s->date
                , $s->isActive
                , $s->modified
                , $s->modifiedBy
                , $s->name
                , $s->pageDescription
                , $s->pageTitle
                , $s->imagePath
                , $s->style
            ])

            ->fromTable(ShowcaseTable::NAME)
            ->orderByField($sortKey, $sortType);

        return $result;
    }

    /**
     * @param int $id
     */
    public function deletePhoto( $id )
    {
        $q = $this->_selectItemById($id);
        $s = Cms::me()->db()->queryAssoc($q, false);
        $showcase = ShowcaseSelector::me()->fetchOne($s, true);

        Path::deleteFile($showcase->imagePath);
    }

    /**
     * @return string Picture name
     */
    public function addPic($id)
    {
        $pic = $this->selectById($id)->imagePath;
        if (!$pic) {
            $pic = $this->_addPic();
        }

        if ($_POST["imageEnable"] === NULL && !$_FILES["file"]["tmp_name"]) {
            Path::deleteFile($pic);
            $pic = "";
        }


        return $pic;
    }

    /**
     * @return string Picture name
     */
    private function _addPic( )
    {
        if ($_FILES["file"]["tmp_name"]) {

            $fileTmp = $_FILES["file"]["tmp_name"];
            $fileName = $_FILES["file"]["name"];

            $srcFile = $fileTmp;
            $dstPath = "/userfiles/showcase/" . Images::nextName($fileName);

            $dstFile = Path::toLocal($dstPath);

            Path::createPathFolders($dstFile);

            Images::createTransform()
                ->inputFile($srcFile)
                ->reduce(990, 450)
                ->exportFileWithInputFormat($dstFile, 100);

            unlink($srcFile);
        }

        return $dstPath ? $dstPath : "";
    }
}
