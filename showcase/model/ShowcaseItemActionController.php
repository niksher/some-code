<?php

namespace progorod\modules\specproject\showcase\model;

use model\images\Images;
use progorod\Cms;
use progorod\db\queries\DbQuery;
use progorod\db\queries\DbQuerySelect;
use progorod\helpers\Path;
use progorod\modules\specproject\showcase\db\models\Showcase;
use progorod\modules\specproject\showcase\db\models\ShowcaseCatSelector;
use progorod\modules\specproject\showcase\db\models\ShowcaseItem;
use progorod\modules\specproject\showcase\db\models\ShowcaseItemSelector;
use progorod\modules\specproject\showcase\db\tables\ShowcaseItemTable;
use progorod\modules\specproject\showcase\db\tables\ShowcaseItemTableFields;
use progorod\modules\specproject\showcase\ShowcaseModule;


class ShowcaseItemActionController
{
    public function __construct( ShowcaseModule $module )
    {
        $this->_module = $module;
    }


    /** @var ShowcaseModule */
    private $_module;

    /**
     * @param Showcase $article
     * @param boolean $buffered
     */
    public function selectFullArticle(Showcase $article, $buffered=false)
    {
        $select = Cms::me()->modules()->showcase()->showcaseCatActions()->_selectCatList($article->id);
        $arr = Cms::me()->db()->queryAssoc($select, $buffered);
        $article->resolvedCats = ShowcaseCatSelector::me()->fetch($arr);

        $this->_fillCatsItems($article, $buffered);
    }

    /**
     * @param int $id
     * @return ShowcaseItem
     */
    public function getById( $id, $buffered=false )
    {
        $select = $this->_selectItemById($id);
        $arr = Cms::me()->db()->queryAssoc($select, $buffered);
        $result = ShowcaseItemSelector::me()->fetchOne($arr);
        return $result;
    }

    /**
     * @param int $catId
     * @return ShowcaseItem
     */
    public function create( $catId )
    {
        $f = new ShowcaseItemTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            INSERT INTO `:table`
            SET 
                `:f_catId` = :catId
                , `:f_isActive` = :isActive
_____
            , [
                "table" => ShowcaseItemTable::NAME
                , "f_catId" => $f->catId
                , "f_isActive" => $f->isActive
                , "catId" => (int)$catId
                , "isActive" => 0
            ]
            , false);

        return Cms::me()->modules()->showcase()->showcaseItemActions()->getById(Cms::me()->db()->lastInsertId());
    }

    /**
     * @param Showcase $showcase
     */
    public function update( Showcase $showcase )
    {		
        $f = new ShowcaseItemTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            UPDATE `:table`
            SET
                `:f_catId` = :catId
                , `:f_isActive` = :isActive
                , `:f_name` = :name
                , `:f_content` = :content
                , `:f_imagePath` = :imagePath
                , `:f_url` = :url
            WHERE `:f_id` = :id
_____
            , [
                "table" => ShowcaseItemTable::NAME
                , "f_id" => $f->id
                , "f_catId" => $f->catId
                , "f_isActive" => $f->isActive
                , "f_name" => $f->name
                , "f_content" => $f->content
                , "f_imagePath" => $f->imagePath
                , "f_url" => $f->url
                , "id" => $showcase->id
                , "catId" => $showcase->catId
                , "isActive" => $showcase->isActive
                , "name" => $showcase->name
                , "content" => $showcase->content
                , "imagePath" => $showcase->imagePath
                , "url" => $showcase->url
            ]
            , false);
    }

    /**
     * @param int $id
     */
    public function removeById($id)
    {
        $this->deletePhoto($id);
        $this->_removeById($id);
    }

    /**
     * @param int $id
     */
    private function _removeById($id)
    {
        $f = new ShowcaseItemTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            DELETE FROM `:table`
            WHERE `:f_id`=:id
_____
            , [
                "table" => ShowcaseItemTable::NAME
                , "f_id" => $f->id
                , "id" => $id
            ]
        );
    }

    /**
     * @param Showcase $showcase
     */
    public function removeByCatId($showcase, $id)
    {
        foreach ($showcase->resolvedCats as $cat) {
            if ($cat->id == $id) {
                foreach ($cat->items as $item) {
                    $this->deletePhoto($item->id);
                    $this->_removeById($item->id);
                }
            }

        }
    }

    /**
     * @param int $id
     */
    public function _removeByCatId($id)
    {
        $f = new ShowcaseItemTableFields();
        Cms::me()->db()->sqlQueryAssoc(<<<_____
            DELETE FROM `:table`
            WHERE `:f_catId`=:catId
_____
            , [
                "table" => ShowcaseItemTable::NAME
                , "f_id" => $f->catId
                , "catId" => $id
            ]
        );
    }

    /**
     * @param array $catIds
     * @return DbQuerySelect
     */
    private function _selectItemsByCats($catIds)
    {
        $s = new ShowcaseItemTableFields();
        $result = DbQuery::select()
            ->select([
                $s->id 
                , $s->catId
                , $s->isActive
                , $s->content
                , $s->name
                , $s->url
                , $s->imagePath
            ])

            ->fromTable(ShowcaseItemTable::NAME)
            ->whereField($s->catId, "IN", $catIds);

        return $result;
    }

    /**
     * @param int $id
     * @return DbQuerySelect
     */
    private function _selectItemById($id)
    {
        $s = new ShowcaseItemTableFields();
        return DbQuery::select()
            ->select([
                $s->id 
                , $s->catId
                , $s->isActive
                , $s->content
                , $s->name
                , $s->url
                , $s->imagePath
            ])

            ->fromTable(ShowcaseItemTable::NAME)
            ->whereField($s->id, "=", $id);
    }

    /**
     * @param Showcase $article
     * @param boolean $buffered
     */
    private function _fillCatsItems(Showcase $article, $buffered )
    {
        $catIds = [];
        foreach($article->resolvedCats as $cat) {
            $catIds[] = $cat->id;	
        }
        if (count($catIds) == 0) {
            return;
        }
        $itemsQuery = $this->_selectItemsByCats($catIds);
        $arr = Cms::me()->db()->queryAssoc($itemsQuery, $buffered);
        $items = ShowcaseItemSelector::me()->fetch($arr);
        foreach ($article->resolvedCats as $cat) {
            foreach ($items as $item) {
                if ($item->catId == $cat->id) {
                    $cat->items[] = $item;
                }
            }
        }
    }

    /**
     * @param int $id
     */
    public function deletePhoto( $id )
    {
        $q = $this->_selectItemById($id);
        $s = Cms::me()->db()->queryAssoc($q, false);
        $showcaseItem = ShowcaseItemSelector::me()->fetchOne($s, true);

        Path::deleteFile($showcaseItem->imagePath);
    }

    /**
     * @var int $id
     * @return string Picture name
     */
    public function addPic( $id )
    {
        $pic = $this->getById($id)->imagePath;
        if (!$pic) {
            $pic = $this->_addPic();
        }

        if ($_POST["imageEnable"] === NULL && !$_FILES["file"]["tmp_name"]) {
            Path::deleteFile($pic);
            $pic = "";
        }


        return $pic;
    }

    /**
     * @return string Picture name
     */
    public function _addPic( )
    {
        if ($_FILES["file"]["tmp_name"]) {

            $fileTmp = $_FILES["file"]["tmp_name"];
            $fileName = $_FILES["file"]["name"];

            $srcFile = $fileTmp;
            $dstPath = "/userfiles/showcase/" . Images::nextName($fileName);

            $dstFile = Path::toLocal($dstPath);

            Path::createPathFolders($dstFile);

            Images::createTransform()
                ->inputFile($srcFile)
                ->reduce(250, 185)
                ->exportFileWithInputFormat($dstFile, 100);

            unlink($srcFile);
        }

        return $dstPath ? $dstPath : "";
    }
}
