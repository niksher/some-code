<?php

namespace progorod\modules\specproject\showcase;

use progorod\modules\BaseModule;
use progorod\modules\specproject\showcase\model\ShowcaseActionController;
use progorod\modules\specproject\showcase\model\ShowcaseCatActionController;
use progorod\modules\specproject\showcase\model\ShowcaseItemActionController;


class ShowcaseModule extends BaseModule
{
    public function __construct()
    {
    }


    /** @var ShowcaseActionController */
    private $_showcaseActions;

    /** @var ShowcaseCatActionController */
    private $_showcaseCatActions;

    /** @var ShowcaseItemActionController */
    private $_showcaseItemActions;


    /**
     * @return ShowcaseActionController
     */
    public function showcaseActions()
    {
        if (!$this->_showcaseActions) {
            $this->_showcaseActions = new ShowcaseActionController($this);
        }
        return $this->_showcaseActions;
    }

    /**
     * @return ShowcaseCatActionController
     */
    public function showcaseCatActions()
    {
        if (!$this->_showcaseCatActions) {
            $this->_showcaseCatActions = new ShowcaseCatActionController($this);
        }
        return $this->_showcaseCatActions;
    }

    /**
     * @return ShowcaseItemActionController
     */
    public function showcaseItemActions()
    {
        if (!$this->_showcaseItemActions) {
            $this->_showcaseItemActions = new ShowcaseItemActionController($this);
        }
        return $this->_showcaseItemActions;
    }
}
