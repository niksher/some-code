<?php

namespace progorod\modules\specproject\showcase;

use progorod\BaseFactory;


class ShowcaseModuleFactory extends BaseFactory
{
    /**
     * @return ShowcaseModule
     */
    public function create( )
    {
        return new ShowcaseModule();
    }
}
